﻿<html>
  <head>
    <title>Home</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link href="resources/css/jquery-ui-themes.css" type="text/css" rel="stylesheet"/>
    <link href="resources/css/axure_rp_page.css" type="text/css" rel="stylesheet"/>
    <link href="data/styles.css" type="text/css" rel="stylesheet"/>
    <link href="files/home/styles.css" type="text/css" rel="stylesheet"/>
    <script src="jquery-1.7.2.js"></script>
    <script src="resources/scripts/jquery-ui-1.8.10.custom.min.js"></script>
    <script src="resources/scripts/axure/axQuery.js"></script>
    <script src="resources/scripts/axure/globals.js"></script>
    <script src="resources/scripts/axutils.js"></script>
    <script src="resources/scripts/axure/annotation.js"></script>
    <script src="resources/scripts/axure/axQuery.std.js"></script>
    <script src="resources/scripts/axure/doc.js"></script>
    <script src="data/document.js"></script>
    <script src="resources/scripts/messagecenter.js"></script>
    <script src="resources/scripts/axure/events.js"></script>
    <script src="resources/scripts/axure/action.js"></script>
    <script src="resources/scripts/axure/expr.js"></script>
    <script src="resources/scripts/axure/geometry.js"></script>
    <script src="resources/scripts/axure/flyout.js"></script>
    <script src="resources/scripts/axure/ie.js"></script>
    <script src="resources/scripts/axure/model.js"></script>
    <script src="resources/scripts/axure/repeater.js"></script>
    <script src="resources/scripts/axure/sto.js"></script>
    <script src="resources/scripts/axure/utils.temp.js"></script>
    <script src="resources/scripts/axure/variables.js"></script>
    <script src="resources/scripts/axure/drag.js"></script>
    <script src="resources/scripts/axure/move.js"></script>
    <script src="resources/scripts/axure/visibility.js"></script>
    <script src="resources/scripts/axure/style.js"></script>
    <script src="resources/scripts/axure/adaptive.js"></script>
    <script src="resources/scripts/axure/tree.js"></script>
    <script src="resources/scripts/axure/init.temp.js"></script>
    <script src="files/home/data.js"></script>
    <script src="resources/scripts/axure/legacy.js"></script>
    <script src="resources/scripts/axure/viewer.js"></script>
    <script type="text/javascript" src="jquery.sparkline.js"></script>
    <script type="text/javascript" src="kens_graphs.js"></script>

    <script type="text/javascript">
      $axure.utils.getTransparentGifPath = function() { return 'resources/images/transparent.gif'; };
      $axure.utils.getOtherPath = function() { return 'resources/Other.html'; };
      $axure.utils.getReloadPath = function() { return 'resources/reload.html'; };
    </script>
  </head>
  <body>
    <div id="base" class="">

      <!-- Unnamed (Vertical Line) -->
      <div id="u0" class="ax_vertical_line">
        <img id="u0_start" class="img " src="resources/images/transparent.gif" alt="u0_start"/>
        <img id="u0_end" class="img " src="resources/images/transparent.gif" alt="u0_end"/>
        <img id="u0_line" class="img " src="images/home/u0_line.png" alt="u0_line"/>
      </div>

      <!-- img_Gross_Margin_Text (Shape) -->
      <div id="u1" class="ax_shape" data-label="img_Gross_Margin_Text">
        <img id="u1_img" class="img " src="images/home/img_gross_margin_text_u1.png"/>
        <!-- Unnamed () -->
        <div id="u2" class="text">
          <p><span>Quick Ratio</span></p>
        </div>
      </div>

      <!-- img_Gross_Target (Shape) -->
      <div id="u3" class="ax_shape" data-label="img_Gross_Target">
        <img id="u3_img" class="img " src="images/home/img_gross_target_u3.png"/>
        <!-- Unnamed () -->
        <div id="u4" class="text">
          <p><span id="kGraph">1,2,-3,4,5,6,4</span></p>
        </div>
      </div>

      <!-- img_EBITDA_Margin (Shape) -->
      <div id="u5" class="ax_shape" data-label="img_EBITDA_Margin">
        <img id="u5_img" class="img " src="images/home/img_gross_margin_text_u1.png"/>
        <!-- Unnamed () -->
        <div id="u6" class="text">
          <p><span>Current Ratio</span></p>
        </div>
      </div>

      <!-- img_EBITDA_Margin_Target (Shape) -->
      <div id="u7" class="ax_shape" data-label="img_EBITDA_Margin_Target">
        <img id="u7_img" class="img " src="images/home/img_gross_target_u3.png"/>
        <!-- Unnamed () -->
        <div id="u8" class="text">
          <p><span id="kGraph">1,2,-3,4,5,6,4</span></p>
        </div>
      </div>

      <!-- img_Gross_Margin_Text (Shape) -->
      <div id="u9" class="ax_shape" data-label="img_Gross_Margin_Text">
        <img id="u9_img" class="img " src="images/home/img_gross_margin_text_u1.png"/>
        <!-- Unnamed () -->
        <div id="u10" class="text">
          <p><span>Times Interest Earned</span></p>
        </div>
      </div>

      <!-- img_Operating_Margin_Target (Shape) -->
      <div id="u11" class="ax_shape" data-label="img_Operating_Margin_Target">
        <img id="u11_img" class="img " src="images/home/img_gross_target_u3.png"/>
        <!-- Unnamed () -->
        <div id="u12" class="text">
          <p><span id="kGraph2">1,7,-3,2,5,2,4</span></p>
        </div>
      </div>
    </div>
  </body>
</html>
