



$(function() {
    $('.inlinesparkline').sparkline(); 

    var myvalues = [10,8,5,-7,4,4,1];
    var myCurrentValue = ['Current Value:  32.567'];

    var myMonths = ['June',
                    'July',
                    'Aug ',
                    'Sept',
                    'Oct ',
                    'Nov ',
                    'Dec '];

    var myVals = [234.15 , 613.73 , -725.00 , 210.01 , 432.52 , 139.47 ,43.76];
    
    $('.dynamicsparkline').sparkline(myvalues);

    $('.dynamicbar').sparkline(myvalues, {type: 'bar', barColor: 'grey', disableTooltips: true} );

    $('.inlinebar').sparkline('html',{  type: 'bar', 
                                        height: 25,
                                        barColor: 'GREY', 
                                        negBarColor: 'red', 
                                        barWidth: 14,
                                        tooltipOffsetY: -40,
                                        tooltipPrefix: 'Rate: '    
                                    } 
                            );
   $('.inlinebar').sparkline(myMonths, {composite: true,  
                                        type: 'bar', 
                                        height: 25,
                                        barWidth: 14, 
                                        fillColor:false, 
                                        barColor:'white', 
                                        tooltipOffsetY: -40,
                                        tooltipPrefix: 'Month: '});

    $('.stackedbar').sparkline('html',{type: 'bar', width: '150px', barWidth: 19, tooltipOffsetY: -50, tooltipOffsetX: -20  });
    $('.stackedbar').sparkline(['Q1','Q2', 'Q3', 'Q4'],{composite: true, type: 'bar', width: '150px', barWidth: 19, disableTooltips: false });

    $('.mouseoverdemo').sparkline(myvalues, {width: "200px", height: "30" , disableTooltips: true} );
    $('.mouseoverregion').text(myCurrentValue);
    $('.mouseoverdemo').bind('sparklineRegionChange', function(ev) {
            var sparkline = ev.sparklines[0],
            region = sparkline.getCurrentRegionFields(),
            value = region.y;
    $('.mouseoverregion').text(myMonths[region.x ] +": "+ value);
    }).bind('mouseleave', function() {      
        $('.mouseoverregion').text(myCurrentValue);
    });
           

    $('#kGraph').sparkline('html', { height: '20px', width: '130px', fillColor: 'MediumSeaGreen', lineColor:'DarkSlateGrey', tooltipPrefix: 'Value: '});
    $('#kGraph').sparkline(myMonths, {composite: true, height: '20px', width: '130px',  type: 'bar', barSpacing: 0, barWidth: 19, barColor: '#SeaGreen', tooltipPrefix: 'Month: '});

    $('#kGraph1').sparkline('html', { height: '20px', width: '130px', fillColor: 'MediumSeaGreen', lineColor:'DarkSlateGrey', tooltipPrefix: 'Value: '});
    $('#kGraph1').sparkline(myMonths, {composite: true, height: '20px', width: '130px',  type: 'bar', barSpacing: 0, barWidth: 19, barColor: '#SeaGreen', tooltipPrefix: 'Month: '});


    $('#kGraph2').sparkline('html', { height: '20px', width: '130px', fillColor: 'white', lineColor:'DarkSlateGrey', tooltipPrefix: 'Value: '});
    $('#kGraph2').sparkline(myMonths, {composite: true, height: '20px', width: '130px',  type: 'bar', barSpacing: 0, barWidth: 19, barColor: '#SeaGreen', tooltipPrefix: 'Month: '});
    

});

